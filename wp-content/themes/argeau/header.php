<!DOCTYPE html>
<html <?php language_attributes(); ?> >

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">

		<header id="header" class="text-white bg-primary">
			<div class="container-fluid">
				<div class="d-flex justify-content-between align-items-center">
					<?php
						if( has_custom_logo() ){
							echo get_custom_logo();
						}
					?>
					<button class="burger d-flex flex-column justify-content-end ">
						<span></span>
						<span></span>
						<span></span>
					</button>
				</div>
			</div>
		</header>

		<div id="content" class="site-content">