        </div><!-- #content -->

		<footer id="footer" class="footer">
            <div class="container">
                <div class="h1 text-center my-5">footer</div>
            <?php
            if ( is_active_sidebar( 'sidebar-1' ) ) : ?>

                <aside class="widget-area">
                    <?php dynamic_sidebar( 'sidebar-1' ); ?>
                </aside><!-- .widget-area -->

            <?php endif; ?>
            </div>
		</footer>

	</div><!-- /Page Holder -->

<?php wp_footer() ?>

</body>
</html>