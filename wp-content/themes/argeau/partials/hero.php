<?php if( have_rows('hero') ): ?>
    <?php while( have_rows('hero') ): the_row(); ?>
        <section class="section-hero bg-primary text-white">
            <div class="bg-holder" style="background-image: url('<?php echo the_sub_field('background') ?>')">
                <div class="container">
                    <?php print_html('%s', get_sub_field('text')); ?>
                    <?php print_html('<div class="d-flex justify-content-center"><button class="arrow bg-transparent border-0"><img src="%s"></button></div>', get_sub_field('button_down')); ?>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>