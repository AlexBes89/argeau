<?php if( have_rows('what_we_do') ): ?>
    <?php while( have_rows('what_we_do') ): the_row(); ?>
        <section class="section-what-we-do">
            <div class="container">
                <?php if( have_rows('items') ): ?>
                    <?php while( have_rows('items') ): the_row(); ?>
                        <div class="row no-gutters">
                            <div class="col-lg-6">
                                <div class="text-holder">
                                    <div class="number">0<?php echo get_row_index(); ?></div>
                                    <?php print_html('<h3 class="title" style="color: %2$s">%1$s</h3>', [get_sub_field('title'), get_sub_field('title_color')]); 
                                    ?>
                                    <?php print_html('<p style="color: %2$s">%1$s</p>', [get_sub_field('text'), get_sub_field('text_color')]); ?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <?php print_html('<img src="%s">', get_sub_field('image')); ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>