<?php
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );
function theme_enqueue_scripts() {
	wp_enqueue_style('app.css', get_template_directory_uri() . '/css/app.css');
	wp_enqueue_script('app.js', get_template_directory_uri() . '/js/app.js', array(), false, true);
}

function print_html($tmpl, $values, $required = 0, $echo = true) {
    if (!empty($values[$required])) {

        $html = vsprintf($tmpl, $values);

        if ($echo) {
            echo $html;
        } else {
            return $html;
        }
    }
}

show_admin_bar( false );

if( function_exists('acf_add_options_page') ) {
	acf_add_options_sub_page(array(
		'title' => 'Site Options',
		'parent' => 'options-general.php',
		'capability' => 'manage_options'
	));
	acf_add_options_sub_page('Test');
}

add_action('after_setup_theme', function () {
	add_theme_support( 'custom-logo' );
}, 20);