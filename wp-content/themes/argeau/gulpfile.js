'use strict';

const babel = require('gulp-babel'),
    cleanCSS = require('gulp-clean-css'),
    gulp = require('gulp'),
    gutil = require('gulp-util'),
    prefixer = require('gulp-autoprefixer'),
    rimraf = require('rimraf'),
    rigger = require('gulp-rigger'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    strip = require('gulp-strip-comments'),
    async = require('async'),
    watch = require('gulp-watch'),
    concat = require('gulp-concat');

function swallowError (error) {

  // If you want details of the error in the console
  console.log(error.toString());

  this.emit('end');
}

const folders = {
    src: 'src/',
    dist: './'
}

const path = {
    build: {
        js: folders.dist + 'js/',
        css: folders.dist + 'css/'
    },
    src: {
        js: folders.src + 'js/*.js',
        css: folders.src + 'styles/app.scss'
    },
    watch: {
        js: folders.src + 'js/**/*.js',
        css: folders.src + 'styles/**/*.scss'
    },
    clean: [folders.dist + 'js', folders.dist + 'css'],
    node: 'node_modules'
};

gulp.task('js:build', () =>
    gulp.src(path.src.js)
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(concat('app.js'))
        .pipe(rigger())
        .pipe(strip())
        .pipe(sourcemaps.init())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('/'))
        .pipe(gulp.dest(path.build.js))
);

gulp.task('styles:build', () =>
    gulp.src(path.src.css)
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: [path.src.css],
            outputStyle: 'compressed',
            sourceMap: false,
            errLogToConsole: true
        }))
        .on('error', swallowError)
        .pipe(prefixer())
        .pipe(cleanCSS({level: {1: {specialComments: 0}}}))
        .pipe(sourcemaps.write('/'))
        .pipe(gulp.dest(path.build.css))
);

gulp.task('clean', (cb) =>
    async.parallel(path.clean.map(function(dir) {
        return function(done) {
            console.log('cleaning: ' + dir);
            rimraf(dir + '/*', done);
        }
    }), cb)
);

gulp.task('build', gulp.parallel(
    'js:build',
    'styles:build'
));

gulp.task('default', gulp.series('build', () => {
    gulp.watch(path.watch.css, gulp.series('styles:build'));
    gulp.watch(path.watch.js, gulp.series('js:build'));
}));